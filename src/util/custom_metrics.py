import pandas as pd
import csv


def get_score_classes(predicted_data_path="data/processed/predicted_test_data.csv", only_average=True):
    """
    This function calculates precision, recall and f1 score for each label in the predicted test data.
    It prints the results on the console, while also writing them to a file called evaluated_score.csv.
    :param predicted_data_path: The path to the predicted data for the evaluation
    :param only_average: If True, only prints average scores at the end
    :return: Average f1 score
    """
    predicted_df = pd.read_csv(predicted_data_path)

    unique_labels = predicted_df["label"].unique()

    this_list = []
    total_prec = 0
    total_rec = 0
    total_num = len(unique_labels)
    label_freq_dict = {}
    for label in unique_labels:
        # get rows where the actual label is the label
        first_subset = predicted_df.loc[predicted_df["label"] == label]
        # get rows where the predicted label is the label
        second_subset = predicted_df.loc[predicted_df["predicted_labels"] == label]

        # calculate TP, FP, FN
        TP = first_subset.loc[first_subset["predicted_labels"] == first_subset["label"]].count()["predicted_labels"]
        FP = second_subset.loc[second_subset["predicted_labels"] != second_subset["label"]].count()["predicted_labels"]
        FN = first_subset.loc[first_subset["predicted_labels"] != first_subset["label"]].count()["predicted_labels"]
        if TP == 0:
            precision = 0
            recall = 0
            f1 = 0
        else:
            precision = TP/(TP+FP)
            recall = TP/(TP+FN)
            f1 = 2 * (precision * recall) / (precision + recall)

        # prints the scores for each class if desired
        if not only_average:
            print(f"Label '{label}': Precision = {precision:.3f}, Recall = {recall:.3f}, F1 score = {f1:.3f}")

        this_list.append((precision, recall, f1))
        total_prec += precision
        total_rec += recall
        label_freq_dict[label] = [precision, recall, f1]

    total_prec /= total_num
    total_rec /= total_num
    f1 = 2 * (total_prec * total_rec) / (total_prec + total_rec)
    print(f"Average: Precision = {total_prec:.3f}, Recall = {total_rec:.3f}, F1 score = {f1:.3f}")

    with open("data/processed/evaluated_score.csv", "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(["class", "precision", "recall", "f1"])
        for key, value in label_freq_dict.items():
            row_data = [key, value[0], value[1], value[2]]
            writer.writerow(row_data)

    return f1


def get_relaxed_score(predicted_data_path="data/processed/predicted_test_data.csv", only_average=True):
    """
    This function calculates precision, recall and f1 score for each label in the predicted test data.
    It prints the results on the console, while also writing them to a file called evaluated_relaxed_score.csv.
    :param predicted_data_path: The path to the predicted data for the evaluation
    :param only_average: If True, only prints average scores at the end
    :return: Average f1 score
    """
    class Node:
        def __init__(self, name):
            self.name = name
            self.children = []

        def add_child(self, child):
            self.children.append(child)

    def construct_tree(file_path):
        root = Node('root')
        with open(file_path, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                parent = root
                for label in row:
                    if label:
                        child = Node(label)
                        if child not in parent.children:
                            parent.add_child(child)
                        parent = child
        return root

    def find_parents(node, label):
        if node.name == label:
            return []
        for child in node.children:
            path = find_parents(child, label)
            if path is not None:
                path.insert(0, node.name)
                return path
        return None

    this_list = []

    tree = construct_tree("data/raw/mappings/taxonomy.csv")

    predicted_df = pd.read_csv(predicted_data_path)
    predicted_df["relaxed_label"] = predicted_df["predicted_labels"]

    unique_labels = predicted_df["label"].unique()
    total_prec = 0
    total_rec = 0
    total_num = len(unique_labels)
    label_freq_dict = {}

    for label in unique_labels:
        # get rows where the actual label is the label
        first_subset = predicted_df.loc[predicted_df["label"] == label]
        # get rows where the predicted label is the label
        second_subset = predicted_df.loc[predicted_df["predicted_labels"] == label]

        FP_df = second_subset.loc[second_subset["predicted_labels"] != second_subset["label"]]
        for index, row in FP_df.iterrows():
            parents = find_parents(tree, row["predicted_labels"])
            for element in parents:
                if element == row["label"]:
                    predicted_df.at[index, "relaxed_label"] = element
                    break

        FN_df = first_subset.loc[first_subset["predicted_labels"] != first_subset["label"]]
        for index, row in FN_df.iterrows():
            parents = find_parents(tree, row["predicted_labels"])
            for element in parents:
                if element == row["label"]:
                    predicted_df.at[index, "relaxed_label"] = element
                    break

        # renew rows where the actual label is the label
        first_subset = predicted_df.loc[predicted_df["label"] == label]
        # renew rows where the predicted label is the label
        second_subset = predicted_df.loc[predicted_df["predicted_labels"] == label]

        # calculate TP, FP, FN
        TP = first_subset.loc[first_subset["relaxed_label"] == first_subset["label"]].count()["relaxed_label"]
        FP = second_subset.loc[second_subset["relaxed_label"] != second_subset["label"]].count()["relaxed_label"]
        FN = first_subset.loc[first_subset["relaxed_label"] != first_subset["label"]].count()["relaxed_label"]

        if TP == 0:
            precision = 0
            recall = 0
            f1 = 0
        else:
            precision = TP/(TP+FP)
            recall = TP/(TP+FN)
            f1 = 2 * (precision * recall) / (precision + recall)

        if not only_average:
            print(f"Label '{label}': Precision = {precision:.3f}, Recall = {recall:.3f}, F1 score = {f1:.3f}")

        total_prec += precision
        total_rec += recall
        this_list.append((precision, recall, f1))

        label_freq_dict[label] = [precision, recall, f1]

    total_prec /= total_num
    total_rec /= total_num
    f1 = 2 * (total_prec * total_rec) / (total_prec + total_rec)
    print(f"Average: Precision = {total_prec:.3f}, Recall = {total_rec:.3f}, F1 score = {f1:.3f}")

    with open("data/processed/evaluated_relaxed_score.csv", "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(["class", "precision", "recall", "f1"])
        for key, value in label_freq_dict.items():
            row_data = [key, value[0], value[1], value[2]]
            writer.writerow(row_data)

    return f1
