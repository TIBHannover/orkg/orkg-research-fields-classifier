import pandas as pd
from argparse import ArgumentParser
import json

import torch
from torch.utils.data import DataLoader
from torch.optim import AdamW

from transformers import AutoTokenizer, AutoModelForSequenceClassification, get_scheduler

import pyarrow as pa
from datasets import Dataset
from tqdm.auto import tqdm


def parse_args():
    """
    This function defines and parses command-line arguments for the script.
    :return: Values of parsed arguments
    """
    parser = ArgumentParser()

    parser.add_argument("-tp", "--train_path",
                        type=str,
                        default="data/processed/training_data.csv",
                        required=False,
                        help="Path to training data"
                        )

    parser.add_argument("-mp", "--model_path",
                        type=str,
                        default=None,
                        required=False,
                        help="Path to model being used"
                        )

    parser.add_argument("-n", "--num_epochs",
                        type=int,
                        default=5,
                        required=False,
                        help="Number of training epochs"
                        )

    parser.add_argument("-lr", "--learning_rate",
                        type=float,
                        default=1e-5,
                        required=False,
                        help="Learning rate for the model"
                        )

    return parser.parse_args()


def save_label_dict(df):
    """
    This function saves the label dictionary mapping labels to ids. Later we will need the label dictionary to convert
    the predictions back to the corresponding labels.
    :param df: The dataframe used for converting the labels
    :return: The label dictionary
    """
    label_dict = {label: i for i, label in enumerate(df["label"].unique())}

    with open('data/raw/mappings/label_dict.json', 'w') as f:
        json.dump(label_dict, f)

    return label_dict


def main(train_path="data/processed/training_data.csv", model_path=None, num_epochs=5, learning_rate=1e-5):
    """
    This function is the main function of the script. It preprocesses the train data and trains the model.
    After each epoch it saves the model to a pytorch file in the models/training directory for later evaluation.
    :param train_path: The path to the training data
    :param model_path: The path to the model if we are not training a model from scratch
    :param num_epochs: The number of epochs to train the model
    :param learning_rate: The learning rate used to train the model
    :return:
    """

    # initialise device and tokenizer
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    tokenizer = AutoTokenizer.from_pretrained('malteos/scincl')

    # initialise train dataframe and remove unnecessary columns
    train_df = pd.read_csv(train_path)
    train_df = train_df[["title", "abstract", "label"]]
    train_df["abstract"] = ["" if pd.isna(abstract) else abstract for abstract in train_df["abstract"]]
    train_df["text"] = [row["title"] + tokenizer.sep_token + (row["abstract"] or "") for index, row in train_df.iterrows()]

    # define label dictionary by giving each label an id
    label_dict = save_label_dict(train_df)
    train_df["label"] = train_df["label"].map(label_dict)

    train_df = train_df[["text", "label"]]

    # convert to HuggingFace dataset
    train_dataset = Dataset(pa.Table.from_pandas(train_df))

    # tokenize the text in the dataset
    def tokenize_function(examples):
        # tokenize the text
        tokenized_examples = tokenizer(
            examples["text"],
            padding="max_length",
            truncation=True,
            max_length=512,
        )

        # pad the attention masks to the same length as the input sequences
        tokenized_examples['attention_mask'] = [torch.cat([torch.tensor(mask), torch.zeros(512 - len(mask))]) for mask in tokenized_examples['attention_mask']]

        return tokenized_examples

    tokenized_train_dataset = train_dataset.map(tokenize_function, batched=True)

    # postprocessing
    # remove unnecessary columns from dataset
    tokenized_train_dataset = tokenized_train_dataset.remove_columns(["text"])

    # rename the label column to labels because the model expects the argument to be named as the latter
    tokenized_train_dataset = tokenized_train_dataset.rename_column("label", "labels")

    # set the format of the dataset to return PyTorch instead of lists
    tokenized_train_dataset.set_format("torch")

    # initialise the model
    model = AutoModelForSequenceClassification.from_pretrained("malteos/scincl", num_labels=len(label_dict)).to(device)

    # load a pretrained model if not starting from scratch
    if model_path:
        model = torch.load(model_path)

    # train the model
    train_dataLoader = DataLoader(tokenized_train_dataset, shuffle=True, batch_size=8)

    # define optimizer with the learning rate and the scheduler
    optimizer = AdamW(model.parameters(), lr=learning_rate)
    num_training_steps = num_epochs * len(train_dataLoader)
    lr_scheduler = get_scheduler(
        name="linear", optimizer=optimizer, num_warmup_steps=0, num_training_steps=num_training_steps
    )

    # training loop
    progress_bar = tqdm(range(num_training_steps))

    model.train()

    for epoch in range(num_epochs):
        print("new epoch " + str(epoch + 1))
        for batch in train_dataLoader:
            batch = {k: v.to(device) for k, v in batch.items()}
            outputs = model(**batch)
            loss = outputs.loss
            loss.backward()

            optimizer.step()
            lr_scheduler.step()
            optimizer.zero_grad()
            progress_bar.update(1)

        torch.save(model, f'models/training/new_model_{epoch+1}.pt')

    print('model trained!')


if __name__ == '__main__':
    args = parse_args()

    tp = args.train_path
    mp = args.model_path
    n = args.num_epochs
    lr = args.learning_rate

    main(tp, mp, n, lr)
