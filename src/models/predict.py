from argparse import ArgumentParser
import pandas as pd
from transformers import AutoTokenizer
import torch
import json

from src.util.data_cleaning_utils import process_abstract


def parse_args():
    """
    This function defines and parses command-line arguments for the script.
    :return: Values of parsed arguments
    """
    parser = ArgumentParser()

    parser.add_argument("-ab", "--abstract",
                        type=str,
                        required=True,
                        help="A paper's abstract. Main source for predicting the label."
                        )

    parser.add_argument("-t", "--title",
                        type=str,
                        default="",
                        required=False,
                        help="A paper's title. Can be used in conjunction with the abstract for the prediction."
                        )

    parser.add_argument("-mp", "--model_path",
                        type=str,
                        default="models/best_model_torchscript.pt",
                        required=False,
                        help="Path to model being used"
                        )

    parser.add_argument("-n", "--n_results",
                        type=int,
                        default=5,
                        required=False,
                        help="The top n labels to be retrieved."
                        )

    return parser.parse_args()


def predict_label(model, q, n_results):
    """
    This function runs a given query through the model and decides on the n most fitting labels.
    :param model: The model used for inference
    :param q: The query that needs labeling
    :param n_results: The number of results that should be retrieved
    :return: The top n labels for the given query
    """
    # run query through model and get logits
    with torch.no_grad():
        outputs = model(**q)

    logits = outputs["logits"]

    # load the label encodings to decode the label indices
    with open("data/raw/mappings/label_dict.json", "r") as f:
        label_dict = json.load(f)

    reverse_label_dict = {v: k for k, v in label_dict.items()}

    # get the indices and scores of the top n_results
    top_n_scores, top_n_indices = torch.topk(logits, k=n_results)

    # convert the indices to labels using the label dictionary
    top_n_labels = [reverse_label_dict[i.item()] for i in top_n_indices[0]]

    return top_n_labels


def main(config=None):
    """
    This function is the main function of the script. It parses the arguments, loads the model, tokenizes the input
    string and runs the prediction.
    :param config: Configuration for the arguments
    :return: The top n labels for the given query
    """
    args = config or parse_args()

    assert args.abstract, "abstract must be provided."

    # get arguments
    abstract = args.abstract
    title = args.title
    n_results = args.n_results
    model_path = args.model_path

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    # load our saved model
    model = torch.jit.load(model_path).to(device)
    # model = torch.jit.load("traced_best_model.pt").to(device)
    # model = torch.load("models/sciNCL/best_model695.pt").to(device)    # old pytorch model

    # tokenize input string
    tokenizer = AutoTokenizer.from_pretrained('malteos/scincl')
    if len(title) > 0:
        abstract = title + tokenizer.sep_token + abstract

    abstract = process_abstract(abstract)

    input_encoding = tokenizer.encode_plus(abstract, padding="max_length", truncation=True, max_length=512, return_tensors="pt").to(device)

    label_list = predict_label(model, input_encoding, n_results)
    print(label_list)
    print('model executed for prediction!')

    return label_list


if __name__ == '__main__':
    main()
