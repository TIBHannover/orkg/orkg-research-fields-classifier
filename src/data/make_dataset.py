from src.data.process_merged_data import MergedData


def main():
    merged_data = MergedData()
    merged_data.run()
    print('dataset created!')


if __name__ == '__main__':
    main()
