import pandas as pd
import matplotlib.pyplot as plt
import os
from src.util.data_cleaning_utils import process_abstract
import src.util.data_sampling as data_sampling

from src.data.process_arxiv_data import ArxivData


class MergedData:
    """
    This class merges the arxiv and orkg datasets and processes the abstracts. It can also visualize the number of NaN
    elements per column in the merged dataset.

    It saves the merged dataset to data_processing/data/merged_data.csv.
    """

    def __init__(self):
        self.arxiv_data = ArxivData()
        self.orkg_df, self.arxiv_df = self.arxiv_data.run()
        self.chunks = pd.read_csv("data/processed/processed_arxiv_data.csv", chunksize=100000)
        self.merge_preparation_path = "data/processed/final_merged_data.csv"

    def run(self) -> None:
        """
        Runs the following methods:
        - merge_datasets
        - process_abstracts
        - visualize_nan_columns
        Saves the merged dataset to data_processing/data/merged_data.csv.
        """
        merged_df = self._merge_datasets()

        # merged_df = self._process_abstracts(merged_df)
        # merged_df.to_csv('data_processing/data/merged_data.csv')
        # self._visualize_nan_columns(merged_df)

        # finally split merged data into train and test data
        data_sampling.main(self.merge_preparation_path)
        print("finished data preparation")

    def _merge_datasets(self) -> pd.DataFrame:
        """
        Merges the arxiv and orkg datasets.
        """
        arxiv_merge_preparation_path = self.merge_preparation_path

        if os.path.exists(arxiv_merge_preparation_path):
            os.remove(arxiv_merge_preparation_path)

        first_one = True
        for i, chunk in enumerate(self.chunks):
            self.arxiv_df = chunk
            self.arxiv_df = self.arxiv_df.rename(columns=
                                                 {"authors": "author",
                                                  "categories": "label",
                                                  "id": "arxiv_id",
                                                  "submitter": "arxiv_submitter",
                                                  "journal-ref": "publisher",
                                                  "report-no": "arxiv_report-no",
                                                  "license": "arxiv_license",
                                                  "versions": "arxiv_versions",
                                                  "update_date": "arxiv_update_date"})
            self.arxiv_df = self.arxiv_df.drop(columns=["Unnamed: 0", "in_orkg_data", "multi_label"])  # not sure which columns will still be there tbh

            self.arxiv_df['source'] = "arxiv"
            self.arxiv_df = self._process_abstracts(self.arxiv_df)

            # break to not add the last chunk twice
            if i == len(self.chunks) - 1:
                break

            self.arxiv_df.to_csv(arxiv_merge_preparation_path, mode="a", index=False, header=first_one)
            first_one = False

        self.orkg_df['source'] = "orkg"
        self.orkg_df = self._process_abstracts(self.orkg_df)
        self.orkg_df.drop(columns=["publication month", "url", "publication year", "publisher", "crossref_field", "semantic_field"], inplace=True)  # not sure which columns will still be there tbh

        self.arxiv_df = self.arxiv_df.query("source == 'arxiv'")
        self.arxiv_df = pd.concat([self.arxiv_df, self.orkg_df])
        self.arxiv_df.to_csv(arxiv_merge_preparation_path, mode="a", index=False, header=False)

        # merged_df = pd.concat([self.orkg_df, self.arxiv_df])
        return self.arxiv_df

    def _process_abstracts(self, merged_df: pd.DataFrame) -> pd.DataFrame:
        """
        processes the abstract texts by removing code elements
        :param merged_df
        :return: the same dataset with processed abstracts
        """
        merged_df['abstract'] = merged_df['abstract'].apply(lambda x: process_abstract(x) if not pd.isna(x) else x)
        return merged_df

    def _visualize_nan_columns(self, merged_df: pd.DataFrame):
        """
        Visualizes the number of NaN elements per column in merged_df.
        """
        columns = merged_df.columns.to_list()
        nan_info = {}
        for column in columns:
            nan_info[column] = merged_df[column].isna().sum()

        plt.figure(figsize=(20, 7))
        plt.bar(range(len(nan_info)), list(nan_info.values()), align='center', color=(0.2, 0.4, 0.6, 0.6))
        plt.xticks(range(len(nan_info)), list(nan_info.keys()))
        plt.xticks(rotation=35)
        plt.title('Number of NaN elements per column in Dataframe '
                  '(overall number of papers is {})'.format(len(merged_df)))
        plt.show()


if __name__ == '__main__':
    merged_data = MergedData()
    merged_data.run()
