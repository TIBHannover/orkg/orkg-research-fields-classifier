# ORKG Research Fields Classifier

## Overview

### Aims
This service aims to streamline the process of adding new paper resources to the ORKG by predicting corresponding research fields. 
It is designed to assist contributors who may not be familiar with the extensive research field taxonomy present in the ORKG, enabling them to save significant amounts of time.
By analysing the abstract of a paper, the service suggests potential research fields that align with the content.
This empowers authors to effortlessly select an appropriate research field without requiring in-depth knowledge of the research field taxonomy.

### Approach
The development of the research field classifier involved the following steps:

1. **Dataset Creation**: A curated dataset was created, comprising papers and their corresponding research fields. The title and abstract of each paper were merged to capture a comprehensive representation of the content.
2. **Text Tokenization**: The merged text was tokenized using the [SciNCL](https://github.com/malteos/scincl) tokeniser. This process converted the text into a sequence of tokens, facilitating further analysis and processing.
3. **Model Training**: A pytorch model was trained using the pre-trained BERT language model, [SciNCL](https://github.com/malteos/scincl), as a baseline. Fine-tuning was performed on the tokenized dataset to train a customized research field classification model.

### Dataset
The dataset creation process consists of the following steps:

1. **Fetching Papers from ORKG**:
   * Initially, we retrieve all papers currently present in the ORKG.
   * This serves as the foundation for our dataset.

2. **Processing Fetched Data and Retrieving Missing Abstracts**:
   * We process the fetched data from ORKG and check for missing abstracts.
   * To retrieve the missing abstracts, we query semantic scholar and crossref.
   * This step ensures that we have comprehensive abstracts for all papers in the dataset.

3. **Expanding the Dataset with [Arxiv Papers](https://www.kaggle.com/datasets/Cornell-University/arxiv)**:
   * Next, we extend the current dataset by fetching additional papers from Arxiv.
   * Arxiv papers already contain abstracts, simplifying the process of increasing the available data.
   * We map the Arxiv labels to ORKG labels to maintain consistency and enrich the dataset.

By following these steps, we create a diverse and comprehensive dataset for training and evaluating our research field classifier.

#### Dataset properties
The original dataset consisted of 2,247,729 papers retrieved and merged. 
To reduce the total number of articles for training and evaluation, we applied the following rules for dataset sampling:
1. Train/Test Split: We divided the dataset into an 80% training set and a 20% test set.
2. Minimum Articles per Label: We excluded labels with fewer than 11 articles from both the training and test sets.
3. Minimum Test Data Size: Labels with fewer than 500 articles were excluded from the test data.
4. Maximum Sample Size per Label: Labels with more than 5000 articles were limited to a sample size of 5000.

#### Dataset distribution
Distribution of high-level taxonomy research fields:

|    |   Arts and humanities  |  Engineering |  Life Sciences  |  Physical Sciences & Mathematics   | Social and Behavioral Sciences |
|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|
| Training data | 11 |  34464 |  19871 |  224600   |  20428 |
| Test data | 0   |  8287  | 4616   |  55765 |  5007  |
| Total data   | 11  |  42751 |  24487 |  280365   |  25435 |

#### Plots
Distribution of all research fields in sampled data:

|                                                                                                                                                                     |                                                                                                                                                                                    |
|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| <img height="150" src="./data/processed/Figure_1.png" width="280"/><figcaption align = "center">Fig.1 - Research Fields Distribution</figcaption> | <img height="150" src="./data/processed/Figure_2.png" width="280"/><br/><figcaption align = "center">Fig.2 - Research Fields Distribution</figcaption> |
|       <img height="150" src="./data/processed/Figure_3.png" width="280"/><figcaption align = "center">Fig.3 - Research Fields Distribution</figcaption>       |       <img height="150" src="./data/processed/Figure_4.png" width="280"/><br/><figcaption align = "center">Fig.4 - Research Fields Distribution</figcaption>       |

#### Evaluation
On the sampled dataset, the model achieves an F1-Score of 69.5%. However, there are opportunities to enhance the evaluation process.

In the experimental setup, we considered only the top-1 result of the service response. Yet, in practical use, users are typically 
presented with multiple options to choose from. Therefore, evaluating the model based on the top-3 or top-5 results could 
lead to more accurate assessments.

Additionally, it is worth noting that the model occasionally predicts a more specific research field than the article is 
labeled as. For example, it may predict "Applied Physics" for a paper labeled as "Physics." While these model-generated 
labels may be correct, they are considered errors in the evaluation process. Taking this factor into account could further 
improve the evaluation.

### Limitations
There are several limitations to consider when training the SciNCL model:

1. **Computation Requirements**: Training the SciNCL model is computationally intensive and cannot be efficiently performed on a CPU. Therefore, a GPU is required to handle the significant computational workload.
2. **Training Time**: Despite using a GPU, the training process still takes a considerable amount of time due to the large volume of data involved. On average, each epoch takes approximately ***4 hours*** to complete. This extended training duration should be taken into account when planning experiments or iterations.
3. **Google Colab Restrictions**: To alleviate the computational burden, a Python notebook can be provided for running the training process on Google Colab with access to a GPU. However, it's important to note that Google Colab requires periodic user verification, which may interrupt or disrupt the training process, making it less ideal for prolonged training sessions.

### Useful Links
* SciNCL: https://github.com/malteos/scincl
* Arxiv papers: https://www.kaggle.com/datasets/Cornell-University/arxiv

## How to Run

### Prerequisites

#### Software Dependencies
* Python version ``^3.7.1``

#### Hardware Resources
* RAM ``~16 GB``
* Storage ``<30 GB`` 
* Processor ``GPU``

### Service Retraining

To rebuild the dataset and retrain the model one simply needs to execute the ``main.py`` file.
A new dataset will be created, the model will be trained and evaluated.
```commandline
git clone https://gitlab.com/TIBHannover/orkg/orkg-research-fields-classifier
cd orkg-research-fields-classifier
pip install -r requirements.txt
```
* download arxiv dataset at https://www.kaggle.com/datasets/Cornell-University/arxiv and place in ``data/raw/arxiv_data``. Make sure it is named ``arxiv-metadata-oai-snapshot.json``.
```commandline
python -m src.main
```

#### Dataset rebuilding
The following command creates only the dataset.
```commandline
python -m src.data.process_merged_data
```

#### Model retraining
The following command only trains the model. If no arguments are specified, default will be used.
When using an existing model as a starting point, specify the path to the PyTorch model using ``-mp "path/to/model"``.
```commandline
python -m src.models.train [-tp <training data path>] [-mp <model path>] [-n <number of epochs>] [-lr <learning rate>]
```

#### Model evaluating
The following command only evaluates the model. If no arguments are specified, default will be used.
```commandline
python -m src.models.evaluate [-tp <test data path>] [-mp <model path>] [-p <precise or relaxed evaluation>]
```

### Service Integration

One can run the service by executing the ``predict.py`` file and specifying an abstract.
Optionally one can specify a title, a path to the model for prediction, and the number of labels that the model should return.
```commandline
git clone https://gitlab.com/TIBHannover/orkg/orkg-research-fields-classifier
cd orkg-research-fields-classifier
pip install -r requirements.txt
python -m src.models.predict -ab <abstract> [-t <title>] [-mp <modelpath>] [-n <number of returned labels>]  
```


## Contribution
This service is developed and maintained by:
* Münch, Quentin <quentin.muench@stud.uni-hannover.de>

in collaboration with
* Abu Ahmad, Raia <raia.abu_ahmad@dfki.de>

under supervision of
* D'Souza, Jennifer <jennifer.dsouza@tib.eu>

## License
[MIT](./LICENSE)
